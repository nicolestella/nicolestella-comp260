﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeeMove : MonoBehaviour {
	
	public float speed = 4.0f; //metres per second
	public float turnSpeed = 180.0f; //degrees per second
	public Transform target1;
	public Transform target2;
	private Vector2 heading = Vector2.right;
	
	private Vector2 direction;
	private float P1Magnitude;
	private float P2Magnitude;
	
	//public parameters with default values
	public float minSpeed, maxSpeed;
	public float minTurnSpeed, maxTurnSpeed;
	
	private Transform target;
	private Vector2 beeHeading;
	
	public ParticleSystem explosionPrefab;
	
	void OnDestroy() {
		//create an explosion at the bee's current position
		ParticleSystem explosion = Instantiate(explosionPrefab);
		explosion.transform.position = transform.position;
		//destroy the particle system when it is over
		Destroy(explosion.gameObject, explosion.duration);
	}
	
	void Start(){
		//find the player to set the target
		PlayerMove p = FindObjectOfType<PlayerMove>();
		target1 = p.transform;
		
		Player2Move p2 = FindObjectOfType<Player2Move>();
		target2 = p2.transform;
		
		//bee initially moves in random direction
		beeHeading = Vector2.right;
		float angle = Random.value * 360;
		beeHeading = beeHeading.Rotate(angle);
		
		//set speed and turnSpeed randomly
		speed = Mathf.Lerp(minSpeed, maxSpeed, Random.value);
		turnSpeed = Mathf.Lerp(minTurnSpeed, maxTurnSpeed, Random.value);
	}
	// Update is called once per frame
	void Update () {	
		P1Magnitude = (target1.transform.position - transform.position).sqrMagnitude;
		P2Magnitude = (target2.transform.position - transform.position).sqrMagnitude;
		
		if(P1Magnitude > P2Magnitude){
			direction = target2.position - transform.position;
			direction = direction.normalized;
		}
		else {
			direction = target1.position - transform.position;
			direction = direction.normalized;
		}
		
		/*
		//get the vector from the bee to the target
		//and normalise it
		if(P1Magnitude < P2Magnitude) {
				direction = target2.position - transform.position;
				direction = direction.normalized;
			}
		else {
			direction = target1.position - transform.position;
			direction = direction.normalized;
		}
		*/
				
		//calculate how much to turn per frame
		float angle = turnSpeed * Time.deltaTime;
		
		//turn left or right
		if(direction.IsOnLeft(heading)) {
			//target on left, rotate anticlockwise
			heading = heading.Rotate(angle);
		}
		else {
			//target on right, rotate clockwise
			heading = heading.Rotate(-angle);
		}
		
		//Vector2 velocity = direction * speed;
		transform.Translate(heading * speed * Time.deltaTime);
	}
	
	void OnDrawGizmos() {
		//draw heading vector in red
		Gizmos.color = Color.red;
		Gizmos.DrawRay(transform.position, heading);
		
		//draw target vector in yellow
		Gizmos.color = Color.yellow;
		
		P1Magnitude = (target1.transform.position - transform.position).sqrMagnitude;
		P2Magnitude = (target2.transform.position - transform.position).sqrMagnitude;
		
		if(P1Magnitude > P2Magnitude){
			direction = target2.position - transform.position;
			direction = direction.normalized;
		}
		else {
			direction = target1.position - transform.position;
			direction = direction.normalized;
		}
		Gizmos.DrawRay(transform.position, direction);
	}
	
}



