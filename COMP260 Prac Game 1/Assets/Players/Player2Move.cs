﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player2Move : MonoBehaviour {

	public Vector2 move;
	public Vector2 velocity;
	
	public float maxSpeed = 5.0f; //in metres per second
	public float acceleration = 5.0f; //in metres per second
	public float brake = 5.0f; //in metres per second 
	public float turnSpeed = 30.0f; //in degrees per second 
	
	private float speed = 0.0f; //in metres per second
	
	private BeeSpawner beeSpawner;
	public float destroyRadius = 1.0f;
	
	void Start() {
		//find the bee spawner and store a reference for later
		beeSpawner = FindObjectOfType<BeeSpawner>();
	}
	
	// Update is called once per frame
	void Update () {		
		if(Input.GetButtonDown("Fire1")) {
			//destroy nearby bees
			beeSpawner.DestroyBees(
				transform.position, destroyRadius);
		}
		
		//the horizontal axis controls the turn
		float turn = Input.GetAxis("Horizontal2");
		
		//turn the car
		transform.Rotate(0, 0, turn * -turnSpeed * speed * Time.deltaTime);
		
		// the vertical axis controls acceleration fwd/back
		float forwards = Input.GetAxis("Vertical2");
		if(forwards > 0){
			//accelerate forwards
			speed = speed + acceleration * Time.deltaTime;
		}
		else if(forwards < 0){
			//accelerate backwards
			speed = speed - acceleration * Time.deltaTime;
		}
		else {
			
			//braking
			if(speed > 0){
				speed = speed - (brake * Time.deltaTime);
			}
			else if (speed < 0){
				speed = speed + (brake * Time.deltaTime);
			}
			
		}
		
		
		//clamp the speed
		speed = Mathf.Clamp(speed, -maxSpeed, maxSpeed);
		
		//compute a vector in the up direction of length speed
		Vector2 velocity = Vector2.up * speed;
		
		//move the object
		transform.Translate(velocity * Time.deltaTime, Space.Self);
	}
}

